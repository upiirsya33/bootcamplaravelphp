@extends('master')
@section('judul')
	Halaman Detail Cast {{$cast->id}}
@endsection
@section('isi')
<h2>Show Cast {{$cast->id}}</h2>
<p>Nama : {{$cast->nama}}</p>
<p>Umur : {{$cast->umur}}</p>
<p>Bio  : {{$cast->bio}}</p>
@endsection
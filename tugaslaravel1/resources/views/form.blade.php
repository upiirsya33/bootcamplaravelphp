@extends('master')
@section('judul')
	Halaman Utama
@endsection
@section('isi')
<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="kirim" method="post">
        @csrf
	<label for="fname">First name:</label><br>
	<input type="text" id="fname" name="fname"><br>
	<label for="lname">Last name:</label><br>
	<input type="text" id="lname" name="lname"><br>
	<p>Gender:</p>
	  <input type="radio" id="male" name="male" value="Male">
	  <label for="male">Male</label><br>
	  <input type="radio" id="female" name="female" value="Female">
	  <label for="female">Female</label><br>
	  <input type="radio" id="other" name="other" value="Other">
	  <label for="other">Other</label><br>  
	<p>Nationality</p>
	<select name="nationality" id="nationality">
	  <option value="male">Indonesian</option>
	  <option value="female">Japan</option>
	  <option value="other">Korean</option>
	</select><br>
	<p>Language Spoken:</p>
	<input type="checkbox" id="ls1" name="ls1" value="Bahasa Indonesia">
	  <label for="ls1">Bahasa Indonesia</label><br>
	  <input type="checkbox" id="ls2" name="ls2" value="English">
	  <label for="ls2">English</label><br>
	  <input type="checkbox" id="ls3" name="ls3" value="Other">
	  <label for="ls3">Other</label><br>
	  <p>Bio:</p>
	  <textarea name = "bio" cols="30" rows="8"></textarea><br><br>
	  <input type="submit" value="kirim">
	</form>
@endsection
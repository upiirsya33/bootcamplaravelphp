@extends('master')
@section('judul')
	Halaman List Film
@endsection
@section('isi')
@auth
<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>
@endauth
<div class="row">
    @foreach ($film as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">{{$item->judul}} ({{$item->tahun}})</h5>
              <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
              @auth
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
              <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
              <form action ="/film/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger">
              </form>
              @endauth
              @guest
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
              @endguest
            </div>
          </div>
    </div>
    @endforeach
</div>

@endsection
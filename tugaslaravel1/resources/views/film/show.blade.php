@extends('master')
@section('judul')
	Halaman Detail Film {{$film->judul}}
@endsection
@section('isi')
<a href="/film" class="btn btn-danger my-2">Kembali</a>
    <div class="row">
        <div class="col">
        <img src="{{asset('poster/'.$film->poster)}}" alt="...">
        <h2>{{$film->judul}} ({{$film->tahun}})</h2>
        <p>{{$film->ringkasan}}</p>
        </div>
    </div>

@endsection
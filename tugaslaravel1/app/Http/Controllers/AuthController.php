<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view("form");
    }

    public function kirim(Request $request){
        $fnama = $request["fname"];
        $lnama = $request["lname"];
        return view("welcome", compact('fnama','lnama'));
    }
}
